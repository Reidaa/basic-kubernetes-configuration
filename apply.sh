set -x

CI_PROJECT_DIR="."

echo "### scripts/gl/gl-helmfile --file $CI_PROJECT_DIR/helmfile.yaml apply --suppress-secrets"
scripts/gl/gl-helmfile --file $CI_PROJECT_DIR/helmfile.yaml apply --suppress-secrets
