set -x

NS="kubernetes-dashboard"
USER="sample-user"

kubectl -n $NS get secret $(kubectl -n $NS get sa/$USER -o jsonpath="{.secrets[0].name}") -o go-template="{{.data.token | base64decode}}"
