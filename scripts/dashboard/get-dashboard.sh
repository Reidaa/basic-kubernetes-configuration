# set -x

NS="kubernetes-dashboard"
PORT="8443"

POD_NAME=$(kubectl get pods -n $NS -l "app.kubernetes.io/name=kubernetes-dashboard,app.kubernetes.io/instance=kubernetes-dashboard" -o jsonpath="{.items[0].metadata.name}")

echo "kubectl -n $NS port-forward $POD_NAME $PORT:8443"
echo "go to https://localhost:$PORT"