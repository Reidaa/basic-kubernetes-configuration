filename=test-resources.yaml

clear

cat <<EOF > $filename
apiVersion: v1
kind: Namespace
metadata:
  name: cert-manager-test
---
apiVersion: cert-manager.io/v1
kind: Issuer
metadata:
  name: test-selfsigned
  namespace: cert-manager-test
spec:
  selfSigned: {}
---
apiVersion: cert-manager.io/v1
kind: Certificate
metadata:
  name: selfsigned-cert
  namespace: cert-manager-test
spec:
  dnsNames:
    - example.com
  secretName: selfsigned-cert-tls
  issuerRef:
    name: test-selfsigned
EOF

echo "### kubectl apply -f $filename"
kubectl apply -f $filename
echo "### kubectl describe certificate -n cert-manager-test"
kubectl describe certificate -n cert-manager-test
echo "### kubectl delete -f $filename"
kubectl delete -f $filename

echo "### rm -rf $filename"
rm -rf $filename